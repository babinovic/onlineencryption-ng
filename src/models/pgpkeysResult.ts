export class PgpkeysResult 
{
    publickey: string = ''
    publickeypath: string = ''
    privatekey: string = ''
    privatekeypath: string = ''
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { PgpkeysRequest } from 'src/models/pgpkeysRequest';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

const API_URL: string = 'http://127.0.0.1:8000/api';

@Injectable({
  providedIn: 'root'
})
export class GenerateService 
{
    constructor(private http: HttpClient,private request: PgpkeysRequest) 
    {
        this.init();
    }

    async init() {
    }

    generateKeys(request: PgpkeysRequest): Observable<PgpkeysRequest> 
    {
        let httpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Cache-Control', 'no-cache, private')
            .set('Allow', 'POST, OPTIONS')      
            .set('Access-Control-Allow-Origin','*');

        let options = 
        {
            headers: httpHeaders
        }; 

        return this.http.post<PgpkeysRequest>(API_URL+'/generate', request,options)
        .pipe(
            catchError(this.handleError)
          );
   }

   private handleError(error: HttpErrorResponse) 
   {
        if (error.error instanceof ErrorEvent) 
        {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
        } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
        'Something bad happened; please try again later.');
    };
}
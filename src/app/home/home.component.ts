import { Component, ElementRef, OnInit } from '@angular/core';
import { GenerateService } from '../generate.service';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { PgpkeysRequest } from 'src/models/pgpkeysRequest';
import { PgpkeysResult } from 'src/models/pgpkeysResult';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit
{
  //Declarations
  email: string; 
  passphrase: string;
  length: string;
  selectedLength: string;
  publickey: string;
  privatekey: string;
  request: PgpkeysRequest; 
  result: PgpkeysResult; 
  pgpkeysForm: FormGroup;
  pgpkeysResultForm: FormGroup;  
  urlPublic: string;
  urlPrivate: string;
  show: boolean = false;
  loading: boolean = false;


  constructor(private generateService: GenerateService, private formBuilder: FormBuilder,private elemRef: ElementRef) 
  {
    this.pgpkeysForm = this.createFormGroup(formBuilder);
    this.pgpkeysResultForm = this.createResultFormGroup(formBuilder);
   
  }

  ngOnInit() 
  {
    this.addValidators();
    this.selectedLength = '2';
  }

  //ajoute les validators sur les champs email et passphrase
  addValidators()
  {
    this.pgpkeysForm.controls['pgpkeysRequest'].get('email').setValidators([Validators.minLength(5), Validators.email]);
    this.pgpkeysForm.controls['pgpkeysRequest'].get('email').updateValueAndValidity();
    this.pgpkeysForm.controls['pgpkeysRequest'].get('passphrase').setValidators([Validators.minLength(8), Validators.required]);
    this.pgpkeysForm.controls['pgpkeysRequest'].get('passphrase').updateValueAndValidity();
  }

  generate()
  {
    this.show = false;
    this.loading = true;
    this.generateService.generateKeys(this.request).subscribe((data)=>
    {
      this.onResponse(data);  
      this.loading = false;
      this.show = true;
    });
  }

  //Creates the forrm group for email and passphhrase
  createFormGroup(formBuilder: FormBuilder) 
  {
    return formBuilder.group({
      pgpkeysRequest: formBuilder.group(new PgpkeysRequest()),
    });
  }

  //Creates the forrm group to display keys
  createResultFormGroup(formBuilder: FormBuilder) 
  {
    return formBuilder.group({
      pgpkeysResult: formBuilder.group(new PgpkeysResult()),
    });
  }

  //au clic sur Generate
  onSubmit() 
  {
    if (this.pgpkeysForm.valid)
    {
      const request: PgpkeysRequest = Object.assign({}, this.pgpkeysForm.value);
      this.request = request;
      this.generate();
    }
  }

  //à la réception des clés
  onResponse(data)
  {
    const result: PgpkeysResult = Object.assign({}, data);
    this.pgpkeysResultForm.get('pgpkeysResult').get('publickey').setValue(result.publickey);
    this.pgpkeysResultForm.get('pgpkeysResult').get('privatekey').setValue(result.privatekey);
    this.show = true;
    this.urlPublic = this.generateKeyFile(result.publickey);
    this.urlPrivate = this.generateKeyFile(result.privatekey); 
  }

  //création des fichiers et sauvegarde urls
  generateKeyFile( content: string)
  {
    var data = new Blob([content], {type: 'text/plain'});
    const url= window.URL.createObjectURL(data);
    return url;    
  }

  //lancement téléchargement clé publique
  downloadPublickey()
  {
    saveAs(this.urlPublic,'publicPGPKey.txt');
  }

  //lancement téléchargement clé privée
  downloadPrivatekey()
  {
    saveAs(this.urlPrivate,'privatePGPKey.txt');
  }

  //RAZ
  revert() 
  {
    this.pgpkeysForm.reset();
    this.pgpkeysForm.reset({ pgpkeysRequest: new PgpkeysRequest() });
    this.pgpkeysResultForm.reset({ pgpkeysResult: new PgpkeysResult() });
    this.show=false;
  }
}
